namespace Notifier.Framework {
    export class Config {
        private static environment: string;
        private static config: any;

        private constructor() {}

        public static get(property: string): any {
            return Config.config[Config.environment][property];
        }

        public static init(environment: string) {
            Config.environment = environment;
            Config.config = {
                prod: {
                    environment: 'prod',
                    urls: {
                        posts: 'https://rombarte.pl/pl/api/blog/list/',
                        post: 'https://rombarte.pl/pl/blog/post/',
                        postsCount: 'https://rombarte.pl/pl/api/blog/count',
                    },
                },
                dev: {
                    environment: 'dev',
                    urls: {
                        posts: 'http://rombarte.local/public/pl/api/blog/list/',
                        post: 'http://rombarte.local/public/pl/blog/post/',
                        postsCount: 'https://rombarte.local/public/pl/api/blog/count',
                    },
                }
            };
        }
    }
}