///<reference path="../Service/ServiceWorker.ts"/>
///<reference path="../Service/PostManagement.ts"/>
///<reference path="Config.ts"/>
///<reference path="../View/PostsList.ts"/>
///<reference path="../View/MainMenu.ts"/>
///<reference path="../Controller/MainMenu.ts"/>
///<reference path="../Service/History.ts"/>
///<reference path="../Controller/SplashScreen.ts"/>
///<reference path="../Service/NativeNotification.ts"/>
///<reference path="../Service/BrowserDetector.ts"/>

namespace Notifier.Framework {
    export class Application {
        public static start() {
            if (Service.BrowserDetector.isIE()) {
                alert('Twoja przeglądarka nie jest kompatybilna z tą aplikacją.');
            }
            Service.ServiceWorker.init();
            Service.History.init(Controller.MainMenu.execute);
            Controller.MainMenu.execute();
            Controller.SplashScreen.execute();
            Service.NativeNotification.show();
        }

        public static refresh() {}

        public static close() {}
    }
}