///<reference path="Config.ts"/>

namespace Notifier.Framework {
    export class Logger {
        public static init() {}

        private constructor() {}

        public static debug(message: string): void {
            if (Config.get('environment') === 'prod') {
                return;
            } else {
                console.error('[DEBUG] ' + message);
            }
        }

        public static error(message: string): void {
            console.error('[ERROR] ' + message);
        }
    }
}