///<reference path="Framework/Application.ts"/>

const ENVIRONMENT: string = 'prod';
const VERSION: string = '2019.08.06';

namespace Notifier {
    Framework.Config.init(ENVIRONMENT);
    Framework.Logger.init();
    Framework.Application.start();
}