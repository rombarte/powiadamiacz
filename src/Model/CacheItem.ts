namespace Notifier.Model {
    export class CacheItem {
        public type: string;
        public data: any;
    }
}