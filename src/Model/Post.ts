namespace Notifier.Model {
    export class Post {
        public id: number;
        public title: string;
        public summary: string;
        public content: string;
        public url: string;
    }
}