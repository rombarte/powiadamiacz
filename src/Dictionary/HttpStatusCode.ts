namespace Notifier.Dictionary {
    export class HttpStatusCode {
        public static OK = 200;
        public static NOT_FOUND = 400;
    }
}