///<reference path="BaseView.ts"/>

namespace Notifier.View {
    export class PostsList extends BaseView {
        private static createSectionNoEntries(): HTMLElement {
            let headerText = document.createElement('h2');
            headerText.innerText = 'Nie pobrano żadnych wpisów';

            let articleContainer = document.createElement('article');
            articleContainer.setAttribute('id', 'article-no-entries');
            articleContainer.appendChild(headerText);

            return articleContainer;
        }

        private static createSectionEntries(): HTMLElement {
            let articleContainer = document.createElement('article');
            articleContainer.setAttribute('id', 'div-entries');

            return articleContainer;
        }

        public static show(): void {
            let container = PostsList.createContainer();
            container.appendChild(PostsList.createHeader('Najnowsze wpisy'));
            container.appendChild(PostsList.createSectionNoEntries());
            container.appendChild(PostsList.createSectionEntries());
            container.appendChild(PostsList.createBackButton());

            PostsList.displayView(container);
        }
    }
}