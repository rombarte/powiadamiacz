///<reference path="BaseView.ts"/>
///<reference path="../Service/ShareService.ts"/>

namespace Notifier.View {
    export class ShareApplication extends BaseView {
        public static show(): void {
            if (Service.ShareService.isEnabled()) {
                Service.ShareService.get('Powiadamiacz', 'Powiadamiacz', 'https://app.rombarte.pl');
            }
        }
    }
}