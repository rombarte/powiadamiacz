///<reference path="BaseView.ts"/>
///<reference path="MainMenu.ts"/>

namespace Notifier.View {
    export class WhereAmI extends BaseView {
        private static injectLibrary(container: HTMLElement): void {
            let mapContainer = document.createElement('div');
            mapContainer.setAttribute('id', 'notifier-view-where-am-i-map');
            container.appendChild(mapContainer);
        }

        private static createMap(size: number = 15, latitude: number = 52.2332, longitude: number = 21.0614): void {
            let mymap = L.map('notifier-view-where-am-i-map');
            mymap.setView([latitude, longitude], size);
            let osmAttrib='Map data © <a href="https://openstreetmap.org">OpenStreetMap</a> contributors';
            L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
                attribution: osmAttrib,
                maxZoom: 18,
                minZoom: 12
            }).addTo(mymap);
            L.marker([latitude, longitude]).addTo(mymap);
        }

        public static show(): void {
            let container = About.createContainer();
            container.appendChild(WhereAmI.createHeader('Sprawdź, gdzie jesteś'));

            document.addEventListener('Notifier.Service.Localisation.Success', (e: CustomEvent) => {
                WhereAmI.injectLibrary(container);
                WhereAmI.createMap(10, e.detail.latitude, e.detail.longitude);
                container.appendChild(WhereAmI.createBackButton());
                View.SplashScreen.hide();
            });

            document.addEventListener('Notifier.Service.Localisation.Error', () => {
                View.SplashScreen.hide();
                View.MainMenu.show();
                WhereAmI.showError('Nie można uzyskać dostępu do lokalizacji urządzenia');
            });

            WhereAmI.displayView(container);
        }
    }
}