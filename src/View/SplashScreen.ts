///<reference path="BaseView.ts"/>
///<reference path="MainMenu.ts"/>

namespace Notifier.View {
    export class SplashScreen extends BaseView {
        private static create(message: string = 'Trwa ładowanie danych...'): HTMLElement {
            let image = document.createElement('h1');
            image.setAttribute('class', 'notifier-view-splash-screen-logo');
            image.innerText = 'P';

            let text = document.createElement('p');
            text.innerText = message;

            let sectionContainer = document.createElement('section');
            sectionContainer.setAttribute('class', 'notifier-view-splash-screen-container');
            addEventListener('Notifier.Service.SplashScreen.Hide', () => {
                if (sectionContainer.parentNode === document.body) {
                    document.body.removeChild(sectionContainer);
                }
            });

            let articleContainer = document.createElement('article');
            articleContainer.appendChild(image);
            articleContainer.appendChild(text);

            sectionContainer.appendChild(articleContainer);

            return sectionContainer;
        }

        public static show(message: string = 'Trwa ładowanie danych...'): void {
            let splashScreen = SplashScreen.create(message);
            document.body.appendChild(splashScreen);
            window.scroll({top:0, left:0,behavior:'smooth'});
        }

        public static hide(time: number = 1000): void {
            setTimeout(() => {
                dispatchEvent(
                    new CustomEvent('Notifier.Service.SplashScreen.Hide', {
                        detail: {}
                    })
                );
            }, time);
        }
    }
}