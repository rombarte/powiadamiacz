///<reference path="BaseView.ts"/>
///<reference path="MainMenu.ts"/>
///<reference path="SplashScreen.ts"/>

namespace Notifier.View {
    export class About extends BaseView {
        private static createSection(): HTMLElement {
            let firstSection = document.createElement('p');
            firstSection.setAttribute('class', 'notifier-view-about-paragraph');
            firstSection.innerText =
                'Poniższa aplikacja PWA (ang. Progressive Web App) powstała w celu poznania tejże technologii.' +
                ' Ma ona charakter eksperementalny. W przypadku odkrycia błędów lub innych spostrzeżeń proszę o wiadomość e-mail na adres: b.romanek@rombarte.pl.';

            let secondSection = document.createElement('p');
            secondSection.setAttribute('class', 'notifier-view-about-paragraph');
            secondSection.innerHTML = 'Wersja aplikacji: ' + VERSION + '. Obecnie nie udostępniam kodu źródłowego aplikacji, ale z czasem ulegnie to zmianie.';

            let thirdSection = document.createElement('p');
            thirdSection.setAttribute('class', 'notifier-view-about-paragraph');
            thirdSection.innerHTML = 'Zdjęcie w stopce: <a href="https://www.publicdomainpictures.net/en/view-image.php?image=150848&amp;picture=portable-computer-and-smartphone" title="Portable Computer And Smartphone" target="_blank" rel="noopener noreferrer">Portable Computer And Smartphone</a>';

            let articleContainer = document.createElement('article');
            articleContainer.setAttribute('class', 'notifier-view-about-content');
            articleContainer.appendChild(firstSection);
            articleContainer.appendChild(secondSection);
            articleContainer.appendChild(thirdSection);

            return articleContainer;
        }

        public static show(): void {
            let container = About.createContainer();
            container.appendChild(About.createHeader('O aplikacji'));
            container.appendChild(About.createSection());
            container.appendChild(About.createBackButton());
            About.displayView(container);
        }
    }
}