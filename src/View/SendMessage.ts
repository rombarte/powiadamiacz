///<reference path="BaseView.ts"/>

namespace Notifier.View {
    export class SendMessage extends BaseView {
        public static show(): void {
            location.href='mailto:b.romanek@rombarte.pl';
        }
    }
}