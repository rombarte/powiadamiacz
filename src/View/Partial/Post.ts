namespace Notifier.View.Partial {
    export abstract class Post {
        public static show(id: number, title: string, summary: string): HTMLElement {
            let article = document.createElement('article');

            let titleContainer = document.createElement('h2');
            titleContainer.setAttribute('class', 'notifier-view-partial-post-header');
            titleContainer.innerText = title;

            let summaryContainer = document.createElement('p');
            summaryContainer.setAttribute('class', 'notifier-view-partial-post-summary');
            summaryContainer.innerHTML = summary + '...';

            let urlContainer = document.createElement('a');
            urlContainer.setAttribute('class', 'notifier-view-partial-post-link');
            urlContainer.innerText = 'Pokaż pełen wpis';
            urlContainer.setAttribute('href', Framework.Config.get('urls').post + id);
            urlContainer.setAttribute('target', '_blank');

            article.appendChild(titleContainer);
            article.appendChild(summaryContainer);
            article.appendChild(urlContainer);

            return article;

        }
    }
}