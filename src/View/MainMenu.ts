///<reference path="../Controller/PostsList.ts"/>
///<reference path="../Controller/About.ts"/>
///<reference path="BaseView.ts"/>
///<reference path="../Controller/WhereAmI.ts"/>
///<reference path="../Controller/SendMessage.ts"/>
///<reference path="../Controller/ShareApplication.ts"/>

namespace Notifier.View {
    export class MainMenu extends BaseView {
        private static createMenuItem(title: string, description: string, id: string, callback: EventListener) {
            let menuTitle = document.createElement('p');
            menuTitle.setAttribute('class', 'notifier-view-main-menu-menu-title');
            menuTitle.innerText = title;
            menuTitle.setAttribute('id', id);

            let menuDescription = document.createElement('p');
            menuDescription.setAttribute('class', 'notifier-view-main-menu-menu-description');
            menuDescription.innerText = description;

            let menuItem = document.createElement('article');
            menuItem.appendChild(menuTitle);
            menuItem.appendChild(menuDescription);
            menuItem.setAttribute('class', 'notifier-view-main-menu-menu-item');
            menuItem.addEventListener('click', callback);

            return menuItem;
        }

        public static show(): void {
            let container = BaseView.createContainer();
            container.appendChild(MainMenu.createHeader('Wybierz, co chcesz zrobić:'));
            container.appendChild(
                MainMenu.createMenuItem(
                    'Zobacz najnowsze wpisy',
                    'Wyświetl najnowsze wpisy z bloga rombarte.pl',
                    'notifier-view-main-menu-latest-entries',
                    Controller.PostsList.execute
                )
            );
            container.appendChild(
                MainMenu.createMenuItem(
                    'Sprawdź, gdzie jesteś',
                    'Wyświetl swoją pozycję na mapie na podstawie danych z Twojego urządzenia',
                    'notifier-view-main-menu-where-am-i',
                    Controller.WhereAmI.execute
                )
            );
            container.appendChild(
                MainMenu.createMenuItem(
                    'Dowiedz się więcej o aplikacji',
                    'Przeczytaj informacje o aplikacji i jej autorze',
                    'notifier-view-main-menu-about',
                    Controller.About.execute
                )
            );
            container.appendChild(
                MainMenu.createMenuItem(
                    'Napisz wiadomość',
                    'Zgłoś błąd w aplikacji albo ciekawą uwagę (zostanie uruchomiony Twój klient poczty)',
                    'notifier-view-main-menu-send-message',
                    Controller.SendMessage.execute
                )
            );
            if (Service.ShareService.isEnabled()) {
                container.appendChild(
                    MainMenu.createMenuItem(
                        'Podziel się aplikacją',
                        'Wyślij link do aplikacji swoim znajomym',
                        'notifier-view-main-menu-share-application',
                        Controller.ShareApplication.execute
                    )
                );
            }
            MainMenu.displayView(container);
        }
    }
}