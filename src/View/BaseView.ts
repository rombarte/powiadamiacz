///<reference path="../Service/Dom.ts"/>

namespace Notifier.View {
    export abstract class BaseView {
        protected static createContainer(): HTMLElement {
            let viewContainer = document.createElement('section');
            viewContainer.setAttribute('id', 'application-section-view');

            return viewContainer;
        }

        protected static createHeader(text: string): HTMLElement {
            let headerText = document.createElement('h1');
            headerText.innerText = text;

            let headerContainer = document.createElement('header');
            headerContainer.appendChild(headerText);

            return headerContainer;
        }

        protected static displayView(newView: HTMLElement): void {
            let oldView = document.getElementById('application-section-view');
            oldView.parentNode.replaceChild(newView, oldView);
        }

        protected static show(): void {}

        public static showSuccess(message: string, time: number = 5000) {
            let article = document.createElement('article');
            let text = document.createElement('h2');
            text.innerText = message;

            article.setAttribute('class', 'message success');
            article.appendChild(text);

            let oldView = document.getElementById('application-section-view');
            oldView.appendChild(article);

            setTimeout(() => {
                let child = Service.Dom.getBySelector('.message.success');
                if (child.parentNode === oldView) {
                    oldView.removeChild(child);
                }
            }, time);
        }

        public static showError(message: string, time: number = 5000) {
            let article = document.createElement('article');
            let text = document.createElement('h2');
            text.innerText = message;

            article.setAttribute('class', 'message error');
            article.appendChild(text);

            let oldView = document.getElementById('application-section-view');
            oldView.appendChild(article);

            setTimeout(() => {
                let child = Service.Dom.getBySelector('.message.error');
                if (child.parentNode === oldView) {
                    oldView.removeChild(child);
                }
            }, time);
        }

        protected static createBackButton(): HTMLElement {
            let articleContainer = document.createElement('a');
            articleContainer.setAttribute('id', 'application-back-button');
            articleContainer.innerText = 'Powrót';
            articleContainer.addEventListener('click', Controller.MainMenu.execute);
            return articleContainer;
        }
    }
}