///<reference path="BaseController.ts"/>
///<reference path="../View/About.ts"/>
///<reference path="../View/SplashScreen.ts"/>

namespace Notifier.Controller {
    export class SplashScreen extends BaseController {
        public static execute() {
            View.SplashScreen.show('Trwa ładowanie aplikacji Powiadamiacz...');
            View.SplashScreen.hide(1000);
        }
    }
}