///<reference path="BaseController.ts"/>
///<reference path="../View/PostsList.ts"/>
///<reference path="../Service/PostManagement.ts"/>
///<reference path="../View/SplashScreen.ts"/>
///<reference path="../Service/Vibration.ts"/>

namespace Notifier.Controller {
    export class PostsList extends BaseController {
        public static execute() {
            View.SplashScreen.show();
            View.PostsList.show();
            Service.PostManagement.loadLatest();
            if (!Service.Online.isOnline()) {
                Service.Vibration.vibrate();
                View.MainMenu.showError('Jesteś obecnie w trybie offline!');
            }
        }
    }
}