///<reference path="BaseController.ts"/>
///<reference path="../View/MainMenu.ts"/>
///<reference path="../Service/Online.ts"/>
///<reference path="../Service/Vibration.ts"/>

namespace Notifier.Controller {
    export class MainMenu extends BaseController {
        public static execute() {
            View.MainMenu.show();
            if (!Service.Online.isOnline()) {
                Service.Vibration.vibrate();
                View.MainMenu.showError('Jesteś obecnie w trybie offline!');
            }
        }
    }
}