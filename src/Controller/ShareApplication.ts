///<reference path="BaseController.ts"/>
///<reference path="../View/About.ts"/>
///<reference path="../View/ShareApplication.ts"/>

namespace Notifier.Controller {
    export class ShareApplication extends BaseController {
        public static execute() {
            View.SplashScreen.show('Poczekaj na otwarcie nowego okna...');
            View.ShareApplication.show();
            View.SplashScreen.hide();
        }
    }
}