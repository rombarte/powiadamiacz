///<reference path="BaseController.ts"/>
///<reference path="../View/About.ts"/>

namespace Notifier.Controller {
    export class SendMessage extends BaseController {
        public static execute() {
            View.SplashScreen.show('Poczekaj na otwarcie nowego okna...');
            View.SendMessage.show();
            View.SplashScreen.hide();
        }
    }
}