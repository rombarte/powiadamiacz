///<reference path="BaseController.ts"/>
///<reference path="../View/About.ts"/>
///<reference path="../Service/Vibration.ts"/>

namespace Notifier.Controller {
    export class About extends BaseController {
        public static execute() {
            View.SplashScreen.show();
            View.About.show();
            View.SplashScreen.hide();
            if (!Service.Online.isOnline()) {
                Service.Vibration.vibrate();
                View.MainMenu.showError('Jesteś obecnie w trybie offline!');
            }
        }
    }
}