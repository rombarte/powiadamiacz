///<reference path="BaseController.ts"/>
///<reference path="../View/About.ts"/>
///<reference path="../Service/Localisation.ts"/>
///<reference path="../View/SplashScreen.ts"/>
///<reference path="../Service/Online.ts"/>
///<reference path="../Service/Vibration.ts"/>

namespace Notifier.Controller {
    export class WhereAmI extends BaseController {
        public static execute() {
            View.SplashScreen.show();
            Service.Localisation.pickData();
            View.WhereAmI.show();
            if (!Service.Online.isOnline()) {
                Service.Vibration.vibrate();
                View.MainMenu.showError('Jesteś obecnie w trybie offline!');
            }
        }
    }
}