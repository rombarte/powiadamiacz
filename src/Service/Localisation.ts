///<reference path="../Model/CacheItem.ts"/>
///<reference path="../Framework/Logger.ts"/>

namespace Notifier.Service {
    export class Localisation {
        public static successCallback(position: Position) {
            document.dispatchEvent(new CustomEvent('Notifier.Service.Localisation.Success', {
                detail: {
                    latitude: position.coords.latitude,
                    longitude: position.coords.longitude
                }
            }));
        }

        public static errorCallback() {
            document.dispatchEvent(new CustomEvent('Notifier.Service.Localisation.Error', {
                detail: {}
            }));
        }

        public static pickData() {
            if ("geolocation" in navigator) {
                let options = {
                    enableHighAccuracy: true,
                    maximumAge: 30000,
                    timeout: 27000
                };

                navigator.geolocation.getCurrentPosition(Localisation.successCallback, Localisation.errorCallback, options);
            } else {
                Localisation.errorCallback();
            }
        }
    }
}