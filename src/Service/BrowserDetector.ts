namespace Notifier.Service {
    export class BrowserDetector {
        public static isIE(): boolean {
            return "ActiveXObject" in window;
        }
    }
}