///<reference path="../View/MainMenu.ts"/>

namespace Notifier.Service {
    export class Online {
        public static isOnline(): boolean {
            return navigator.onLine;
        }
    }
}