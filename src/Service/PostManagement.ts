///<reference path="../Framework/Application.ts"/>
///<reference path="Dom.ts"/>
///<reference path="Cache.ts"/>
///<reference path="Request.ts"/>
///<reference path="../View/Partial/Post.ts"/>
///<reference path="../View/SplashScreen.ts"/>

namespace Notifier.Service {
    export class PostManagement {
        static loadLatest(page = 1) {
            Request.get(
                Framework.Config.get('urls').posts + page,
                (response: string) => {
                    let posts = JSON.parse(response);
                    Cache.set('posts', posts);
                    PostManagement.renderList(posts);
                    PostManagement.showList();
                    PostManagement.setHandler('article a', 'click', function () {
                        let link = this.getAttribute('location');
                        PostManagement.open(link);
                    });
                    View.SplashScreen.hide();
                },
                () => {
                    let posts = Cache.get('posts');
                    PostManagement.renderList(posts);
                    PostManagement.showList();
                    View.SplashScreen.hide();
                }
            );
        }

        static showList() {
            Dom.getBySelector('#application-section-view').style.display = 'block';
        }

        static showPost() {
            Dom.getBySelector('#application-section-view').style.display = 'none';
        }

        static renderList(posts: Array<Model.Post>) {
            Dom.getBySelector('#div-entries').innerHTML = '';
            for (let i = 0; i < posts.length; i++) {
                let article = View.Partial.Post.show(
                    posts[i].id,
                    posts[i].title,
                    posts[i].summary
                );
                Dom.getBySelector('#div-entries').appendChild(article);
            }
            Dom.getBySelector('#article-no-entries').style.display = 'none';
        }

        static renderPost(post: Model.Post) {
            let entry = `
            <h2>${post.title}</h2>
            <div>${post.content}</div>
        `;
            Dom.getBySelector('#article-entry').innerHTML = entry;
        }

        static open(postUrl: string) {
            Request.get(
                postUrl,
                (response: any) => {
                    let post = JSON.parse(response);
                    PostManagement.renderPost(post);
                    PostManagement.showPost();
                },
                () => {
                    let post = Cache.get('post');
                    PostManagement.renderPost(post);
                    PostManagement.showPost();
                }
            );
        }

        static allowedEvents() {
            return ['click', 'load'];
        }

        static setHandler(selector: string, event: string, handler: any) {
            if (this.allowedEvents().indexOf(event) === -1) {
                console.log('[Error] Not allowed event.');
                return;
            }
            Framework.Logger.debug('Handler is setup.');
            let elements = document.querySelectorAll(selector);
            for (let i = 0; i < elements.length; i++) {
                elements[i].addEventListener(event, handler);
            }
        }
    }
}