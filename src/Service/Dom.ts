namespace Notifier.Service {
    export class Dom {
        public static getBySelector(selector: string): HTMLElement {
            return <HTMLElement>document.querySelector(selector);
        }
    }
}