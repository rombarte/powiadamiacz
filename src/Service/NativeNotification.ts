///<reference path="../Model/CacheItem.ts"/>
///<reference path="../Framework/Logger.ts"/>
///<reference path="Cache.ts"/>

namespace Notifier.Service {
    export class NativeNotification {
        public static show() {
            let isNotificationFeatureEnabled = "Notification" in window;
            if (!isNotificationFeatureEnabled) {
                View.MainMenu.showError("To urządzenie nie wspiera funkcji powiadomień");
            }

            if (Notification.permission !== 'denied') {
                NativeNotification.push();
            }
        }

        private static push() {
            Notification.requestPermission(function (permission) {
                if (permission === "granted") {
                    navigator.serviceWorker.ready.then(function(registration: ServiceWorkerRegistration) {
                        Request.get(
                            Framework.Config.get('urls').postsCount,
                            (response: string) => {
                                let res = JSON.parse(response);
                                Cache.set('postCount', {count: res.count});
                                registration.showNotification('Ilość postów na stronie', {
                                    body: 'Na stronie jest łącznie ' + res.count + ' postów.',
                                    icon: './img/icon.png',
                                    vibrate: [200, 100, 200, 100, 200, 100, 200],
                                    tag: 'posts-count'
                                });
                            },
                            () => {
                                View.MainMenu.showError('Nie można pobrać liczby postów');
                            }
                        );
                    });
                } else {
                    View.MainMenu.showError('Na Twoim urządzeniu zablokowano powiadomienia');
                }
            });
        }
    }
}