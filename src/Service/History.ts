///<reference path="../Controller/MainMenu.ts"/>

namespace Notifier.Service {
    export class History {
        private static pushEmptyState(): void {
            history.pushState({}, '');
        }

        public static init(afterInitCallback: Function) {
            History.pushEmptyState();
            addEventListener('popstate', () => {
                History.pushEmptyState();
                afterInitCallback();
            });
        }
    }
}