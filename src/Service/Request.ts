///<reference path="../Framework/Config.ts"/>
///<reference path="../Dictionary/HttpStatusCode.ts"/>

namespace Notifier.Service {
    export class Request {
        public static post(url: string, form: HTMLFormElement, successCallback: any, failureCallback: any) {
            let xhr = new XMLHttpRequest();
            let formData = new FormData(form);
            xhr.open('POST', url);
            xhr.onload = function () {
                if (xhr.status === Dictionary.HttpStatusCode.OK) {
                    if (successCallback) {
                        successCallback(xhr.response);
                    }
                } else if (xhr.status !== Dictionary.HttpStatusCode.OK) {
                    if (failureCallback) {
                        failureCallback(xhr.response);
                    }
                }
            };
            xhr.onerror = function () {
                if (failureCallback) {
                    failureCallback(xhr.response);
                }
            };
            if (Framework.Config.get('environment') === 'prod') {
               //  xhr.withCredentials = true;
            }
            xhr.send(formData);
        }

        static get(url: string, successCallback: any, failureCallback: any) {
            let xhr = new XMLHttpRequest();
            xhr.open('GET', url);
            xhr.onload = function () {
                if (xhr.status === Dictionary.HttpStatusCode.OK) {
                    if (successCallback) {
                        successCallback(xhr.response);
                    }
                } else if (xhr.status !== Dictionary.HttpStatusCode.OK) {
                    if (failureCallback) {
                        failureCallback(xhr.response);
                    }
                }
            };
            xhr.onerror = function () {
                if (failureCallback) {
                    failureCallback(xhr.response);
                }
            };
            if (Framework.Config.get('environment') === 'prod') {
                // xhr.withCredentials = true;
            }
            xhr.send();
        }
    }
}