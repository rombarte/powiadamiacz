namespace Notifier.Service {
    export class Vibration {
        public static vibrate(time: number = 500) {
            navigator.vibrate([time]);
        }
    }
}