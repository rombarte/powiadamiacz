namespace Notifier.Service {
    export class ShareService {
        public static isEnabled(): boolean {
            return typeof (<any>navigator).share === 'function';
        }

        public static get(title: string, text: string, url: string) {
            (<any>navigator).share({
                title: title,
                text: text,
                url: url,
            });
        }
    }
}