///<reference path="../Model/CacheItem.ts"/>
///<reference path="../Framework/Logger.ts"/>

namespace Notifier.Service {
    export class Cache {
        private static getStorage(): Storage {
            return localStorage;
        }

        static set(key: string, data: any): void {
            let value = new Model.CacheItem();
            if (typeof data === 'object') {
                value.type = 'object';
                value.data = data;
            } else {
                Framework.Logger.error('Unexpected object type.');
            }
            Cache.getStorage().setItem(key, JSON.stringify(value));
        }

        static get(key: string): any {
            let value = Cache.getStorage().getItem(key);
            let data = JSON.parse(value);
            if (data.hasOwnProperty('type') && typeof data.type === 'object') {
                return JSON.parse(data.data);
            }
            Framework.Logger.error('Unexpected object type.');
        }
    }
}