///<reference path="../Framework/Logger.ts"/>

namespace Notifier.Service {
    export class ServiceWorker {
        public static init() {
            let isServiceWorkersSupport = ('serviceWorker' in navigator);
            if (isServiceWorkersSupport) {
                navigator.serviceWorker.register('/sw.js').then(function () {
                    Framework.Logger.debug('ServiceWorker registered correctly.')
                }).catch(function (error: string) {
                    Framework.Logger.debug('ServiceWorker registration failure: ' + error)
                });
            }
        }
    }
}