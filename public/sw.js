const CACHE_NAME = 'app.rombarte.pl';

let filesToCache = [
    '/css/app.css',
    '/app.js',
    '/img/icon.png',
    '/index.html',
];

self.addEventListener('install', evt => {
    evt.waitUntil(
        caches.open(CACHE_NAME).then(cache => cache.addAll(filesToCache)).catch(err => console.log(err.message))
    );
});

self.addEventListener('fetch', evt => {
    evt.respondWith(
        fetch(evt.request).catch(() => caches.match(evt.request))
    );
});