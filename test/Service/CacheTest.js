const assert = require('assert');
import Cache from "../../src/Service/Cache";

describe('Cache', function () {
    describe('set', function () {
        it('should save object in localStorage', function () {
            Cache.set('key', {value: 1000});

            let value = JSON.parse(localStorage.getItem('key'));
            assert.equal(value, {type: 'object', data: {value: 1000}});
        });
    });
});